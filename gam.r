#local

library(nlme)
library(mgcv)
library(carData)
library(car)
VEG <- read.csv("C:/gam/vegetacion.csv")
VEG.gam1 <- gam(SR ~ s(ROCK), data = VEG)
plot(SR ~ ROCK, data = VEG,
     pch = 16,
     main="Hennessy Becerra - UNIGIS 2020",
     xlab = "% Roca en Substrato",
     ylab = "Riqueza de Especies")
x <- seq(min(VEG$ROCK), max(VEG$ROCK), l=100) 
y <- predict(VEG.gam1, data.frame(ROCK = x), se = TRUE)  
lines(x, y$fit)
lines(x, y$fit + 2 * y$se.fit, lty = 2) 
lines(x, y$fit - 2 * y$se.fit, lty = 2)

#online

library(nlme)
library(mgcv)
library(carData)
library(car)
VEG <- read.csv("http://hbecerra.com/files/vegetacion.csv")
VEG.gam1 <- gam(SR ~ s(ROCK), data = VEG)
plot(SR ~ ROCK, data = VEG,
     pch = 16,
     main="Hennessy Becerra - UNIGIS 2020",
     xlab = "% Roca en Substrato",
     ylab = "Riqueza de Especies")
x <- seq(min(VEG$ROCK), max(VEG$ROCK), l=100) 
y <- predict(VEG.gam1, data.frame(ROCK = x), se = TRUE)  
lines(x, y$fit)
lines(x, y$fit + 2 * y$se.fit, lty = 2) 
lines(x, y$fit - 2 * y$se.fit, lty = 2)