#local

library(sf)
library(sp)
library(raster)
library(gstat)
DTM <- read.table("C:/dtm/dtm.txt", header = TRUE, col.names = c("x", "y", "z"))
MARCO <- DTM
coordinates(MARCO) <- ~x + y
GRID <- raster(extent(MARCO), ncol = 300, nrow = 300, vals = NA)
GRID <- as(GRID, "SpatialGrid")
idw <- idw(DTM$z ~ 1, locations = MARCO, newdata = GRID, idp = 2)
spplot(idw["var1.pred"], main = "IDW (pow = 2) MDT - Mazatlán, México
Hennessy Becerra - UNIGIS 2020")

#online

library(sf)
library(sp)
library(raster)
library(gstat)
DTM <- read.table("http://hbecerra.com/files/dtm.txt", header = TRUE, col.names = c("x", "y", "z"))
MARCO <- DTM
coordinates(MARCO) <- ~x + y
GRID <- raster(extent(MARCO), ncol = 300, nrow = 300, vals = NA)
GRID <- as(GRID, "SpatialGrid")
idw <- idw(DTM$z ~ 1, locations = MARCO, newdata = GRID, idp = 2)
spplot(idw["var1.pred"], main = "IDW (pow = 2) MDT - Mazatlán, México
Hennessy Becerra - UNIGIS 2020")